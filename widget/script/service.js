function Service() {

}
service = new Service();

Service.prototype.get = function(name) {
  var obj = {};
  var dict = Service.prototype.DICT[name];
  if(dict) {
    if(typeof dict == 'string') {
      obj = {
        url: dict,
        method: 'GET'
      }
    } else {
      obj = app.extend({}, dict);
    }
  }
  return obj;
}

Service.prototype.DICT = {
  'login': {
    url: '/app/User/login',
    method: 'POST'
  },

  'init.gethomedata': '/app/init/gethomedata',
  'init.getbasedata': '/app/init/getbasedata',
  
  'me.need_push': {
    url: '/app/user/need_push',
    method: 'POST'
  }
};
