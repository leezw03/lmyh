
apiready = function() {
  app.ready();
  init();
  api.setCustomRefreshHeaderInfo({
    bgColor : '#ffffff'
  }, function() {
    init();
    api.refreshHeaderLoadDone();
  });
}

function init() {
  app.ajax(app.extend({
    showMask: true
  }, service.get('init.gethomedata')), function(ret, err) {
    if (ret && ret.errcode == 0) {
      var data = ret.content;
      initViewBanner(data.banners);
      initViewActivity(data.activity);
      initViewGoods(data.goods);
      //initViewTags(data.tags);
    }
  });
}

function initViewBanner(banners) {
  var bannerEl = $api.dom('#container-banner');
  if(banners && banners.length > 0) {
    var tpl = doT.template($api.dom('#tpl_banner').innerHTML);
    bannerEl.innerHTML = tpl(banners);
    var slide = new auiSlide({
        container: bannerEl,
        height:180,
        speed:300,
        pageShow:true,
        autoPlay: 3000,
        pageStyle:'dot',
        loop:true,
        dotPosition:'center',
        currentPage: function(index) {

        }
    })
    $api.removeCls(bannerEl, 'aui-hide');
  } else {
    bannerEl.innerHTML = '';
    $api.addCls(bannerEl, 'aui-hide');
  }
}
function onPollPicClick(pic) {
  alert(JSON.stringify(pic.action));
}

function initViewActivity(activity) {
  var container = $api.dom('#container-activity');
  if(activity && activity.length > 0) {
    var tpl = doT.template($api.dom('#tpl_activity').innerHTML);
    container.innerHTML = tpl(activity);
    $api.removeCls(container, 'aui-hide');
  } else {
    container.innerHTML = '';
    $api.addCls(container, 'aui-hide');
  }
}

var needTimer = false;
var goodsStore = {};
var timer;
function initViewGoods(goods) {
  var container = $api.dom('#container-goods');
  goodsStore = {};
  if(goods && goods.length > 0) {
    var now = (new Date()).getTime()/1000;
    for(var i = 0; i <goods.length; i++) {
      var data = goods[i];
      var tipsObj = getGoodsTip(data);
      data.tips = tipsObj.tips;
      data.status = tipsObj.status;
      data.clock = tipsObj.clock;
      goodsStore[data.goods_id] = data;
      if(tipsObj.clock) {
        needTimer = true;
      }
    }
    var tpl = doT.template($api.dom('#tpl_goods').innerHTML);
    container.innerHTML = tpl(goods);
    console.info(container.innerHTML);
    $api.removeCls(container, 'aui-hide');
  } else {
    container.innerHTML = '';
    $api.addCls(container, 'aui-hide');
  }
  goodsClock();
}

function getGoodsTip(data) {
  var obj = {};
  var now = (new Date()).getTime()/1000;
  var tips = '';
  var clock = false;
  var status;
  if(data.date_start > now) {
    tips = app.dateCountDown(data.date_start-now)+'后开始';
    status = 'wait';
    clock = true;
  } else if(data.date_end <= 0) {
    tips = '活动已结束';
    status = 'end';
  } else {
    if(data.out_sale == 1) {
      tips = '产品已售罄';
      status = 'empty';
    } else {
      tips = '剩余:'+app.dateCountDown(data.date_end);
      status = 'start';
      clock = true;
    }
  }
  obj.tips = tips;
  obj.status = status;
  obj.clock = clock;
  return obj;
}

function goodsClock() {
  if(needTimer) {
    needTimer = false;
    if(timer) {
      window.clearTimeout(timer);
    }
    timer = window.setTimeout(function() {
      var container = $api.dom('#container-goods');
      var goodsElList = $api.domAll(container, '.container-goods');
      for(var i = 0; i < goodsElList.length; i++) {
        var goodsEl = goodsElList[i];
        var goods_id = goodsEl.getAttribute('goods_id');
        var data = goodsStore[goods_id];
        if(data) {
          if(data.clock) {
            if(data.status = 'wait') {
              data.date_end--;
            }
            var tplsObj = getGoodsTip(data);
            var goodsButton = $api.dom(goodsEl, '.goods-button');
            data.status = tplsObj.status;
            data.tips = tplsObj.tips;
            $api.dom(goodsEl, '.goods-tips').innerHTML = data.tips;
            if(tplsObj.clock) {
              needTimer = true;
            }
            if(data.status == 'start' || data.status == 'wait' ) {
              $api.addCls(goodsButton, 'active');
            } else {
              $api.removeCls(goodsButton, 'active');
            }
          }
        }
      }
      if(needTimer) {
        goodsClock();
      }
    }, 1000);
  }
}

function initViewTags(tags) {
  var container = $api.dom('#container-tags');
  if(tags && tags.length > 0) {
    var tpl = doT.template($api.dom('#tpl_tags').innerHTML);
    container.innerHTML = tpl(tags);
    $api.removeCls(container, 'aui-hide');
  } else {
    container.innerHTML = '';
    $api.addCls(container, 'aui-hide');
  }
}
function openGoods(btn, id) {
  var active = $api.hasCls(btn, 'active');
  if(!active) {
    return;
  }
  alert(id);
}
