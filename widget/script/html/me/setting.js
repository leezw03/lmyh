apiready = function() {
    app.ready();

    app.addEvt($api.dom('#btn-clearcache'), 'click', function() {
      api.confirm({
          msg: '是否清除所有缓存？',
          buttons: ['取消', '确定']
      }, function(ret) {
        if (ret.buttonIndex == 2) {
          app.clearCache();
          api.toast({
              msg: '已清空',
              duration: 2000
          });
        }
      });
    });

    app.addEvt($api.dom('#btn-switch-message'), 'click', function() {
        var isActive = $api.hasCls(this, 'active');
        var me = this;
        if (isActive) {
            $api.removeCls(me, 'active');
        } else {
            $api.addCls(me, 'active');
        }
        app.ajax(app.extend({
          showMask: false,
          data: {
            values: {
              access_token: app.getAccessToken(),
              need_push: isActive?0:1
            }
          }
        }, service.get('me.need_push')), function(ret, err) {
          if(ret.errcode == 0) {
            if (isActive) {
                $api.removeCls(me, 'active');
            } else {
                $api.addCls(me, 'active');
            }
          } else {
            api.toast({
                msg: '设置失败',
                duration: 2000,
                location: 'bottom'
            });
          }
        });
    });

    app.addEvt($api.dom('#btn-loginout'), 'click', function() {
        api.confirm({
            title: '退出登录',
            msg: '确认退出当前账号？',
            buttons: ['取消', '确定']
        }, function(ret) {
            if (ret.buttonIndex == 2) {
                app.setAccessToken(null);
                api.sendEvent({
                    name: 'closeToRoot',
                    extra: {}
                });

                api.closeToWin({
                    name: 'root'
                });
            }
        });
    });
}
