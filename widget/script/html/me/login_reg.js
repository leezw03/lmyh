apiready = function() {
    app.ready();

    app.addEvt($api.dom('#btn-register'), 'click', function() {
      app.openWin({
          name: 'my.register',
          url: 'widget://html/me/register.html'
      });
    });

    app.addEvt($api.dom('#btn-login_psw'), 'click', function() {
      app.openWin({
          name: 'my.login_psw',
          url: 'widget://html/me/login_psw.html'
      });
    });
};
