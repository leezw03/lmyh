apiready = function() {
    app.ready();

    app.addEvt($api.dom('#btn-setting'), 'click', function() {
        app.openWin({
            name: 'me.setting',
            url: 'widget://html/me/setting.html'
        });
    });

    init();

    api.addEventListener({
        name: 'baseInfo'
    }, function(ret, err) {
        if (ret) {
            var info = ret.value;
            setUserInfo(info.customer);
        }
    });
}

function init() {
    var info = $api.getStorage('baseInfo');
    if (info) {
        info = eval(info);
        setUserInfo(info.customer);
    }
    api.sendEvent({
        name: 'refreshBaseInfo'
    });
}

function setUserInfo(user) {
    $api.dom('#label-nickname').innerHTML = user.nickname || '';
    $api.dom('#label-phone').innerHTML = user.phone || '';
    $api.dom('#label-group_name').innerHTML = user.group_name || '';
    $api.dom('#label-pname').innerHTML = user.pname || '';
    if (user.avater) {
        $api.dom('#label-avater').src = user.avater || '';
    }
}
