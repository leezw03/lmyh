apiready = function() {
    app.ready();
    var lastBackKeyTime = null;
    api.addEventListener({
        name: 'keyback'
    }, function(ret, err) {
        var now = new Date();
        var nowTime = now.getTime();
        if (lastBackKeyTime) {
            var pastTime = nowTime - lastBackKeyTime;
            if (pastTime < 3000) {
                api.closeWidget({
                    id: api.appId,
                    retData: {
                        name: 'closeWidget'
                    },
                    silent: true
                });
                return;
            }
        }
        api.toast({
            msg: '再按一次返回键退出！',
            duration: 2000,
            location: 'bottom'
        });
        lastBackKeyTime = nowTime;
    });
    api.addEventListener({
        name: 'closeToRoot'
    }, function(ret, err) {
        setActiveFrame(0);
    });
    api.addEventListener({
        name: 'refreshBaseInfo'
    }, function(ret, err) {
        refreshBaseInfo();
    });
    initFrame();
    refreshBaseInfo();
};
function initFrame() {
  var footerLis = $api.domAll('#footer li');
  var frames = [];
  for(var i = 0; i < footerLis.length; i++) {
    var li = footerLis[i];
    frames.push({
        name: 'frame'+i,
        url: li.getAttribute('frame')
    });
  }
  api.closeFrameGroup({
    name: 'group'
  });
  api.openFrameGroup({
      name: 'group',
      scrollEnabled: false,
      rect: {
          x: 0,
          y: 0,
          w: api.winWidth,
          h: api.winHeight-50
      },
      index: 0,
      frames: frames
  }, function (ret, err) {

  });
}
function toggleFrame(tag) {
  if( $api.hasCls(tag, 'active')) return;
  var footerLis = $api.domAll('#footer li'),
      index = 0;
  for (var i = 0; i < footerLis.length; i++) {
      if( tag == footerLis[i] ){
          index = i;
      }
  }
  if(index > 0) {
    var accessToken = app.getAccessToken();
    if(!accessToken) {
      app.openWin({
          name: 'my.login_reg',
          url: 'widget://html/me/login_reg.html'
      });
      return;
    }
  }
  $api.removeCls($api.dom('#footer li.active'), 'active');
  $api.addCls( footerLis[index], 'active');
  api.setFrameGroupIndex({
      name: 'group',
      index: index
  });
}
function setActiveFrame(index) {
  var footerLis = $api.domAll('#footer li');
  for (var i = 0; i < footerLis.length; i++) {
      if( index == i ){
        $api.addCls(footerLis[i], 'active');
        api.setFrameGroupIndex({
            name: 'group',
            index: index
        });
      } else {
        $api.removeCls(footerLis[i], 'active');
      }
  }
}
function refreshBaseInfo() {
  app.ajax(app.extend({
      showMask: false,
      data: {
          values: {
              access_token: app.getAccessToken()
          }
      }
  }, service.get('init.getbasedata')), function(ret, err) {
      if (ret && ret.errcode == 0) {
          var info = ret.content;
          $api.setStorage('baseInfo', info);
          info = eval(info);
          var user = info.customer;
          var cartNumEl = $api.dom('#badge-cartNum');
          var cartNum = user.cart_num;
          $api.dom(cartNumEl, 'span').innerHTML = cartNum;
          if(cartNum > 0) {
            $api.removeCls(cartNumEl, 'zero');
          } else {
            $api.addCls(cartNumEl, 'zero');
          }
          api.sendEvent({
              name: 'baseInfo',
              extra: info
          });
      }
  });
}
